//
//  DeclarativeBGApp.swift
//  DeclarativeBG
//
//  Created by Manuel Meyer on 10.05.22.
//

import SwiftUI

var store: Backgammon.Store = Backgammon.Store(game: Backgammon())

@main
struct DeclarativeBGApp: App {
    var viewState = ViewState(store:store)
    var body: some Scene {
        WindowGroup {
            ContentView(viewState: viewState)
        }
    }
}
