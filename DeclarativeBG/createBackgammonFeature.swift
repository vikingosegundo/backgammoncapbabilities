//
//  createBackgammonFeature.swift
//  DeclarativeBG
//
//  Created by Manuel Meyer on 10.05.22.
//
import Foundation

enum CapabilityDescriptor: Equatable {
    case `switch`(to:Backgammon.Color)
    case roll
    case startNewGame
    case move(Backgammon.Color, from:Backgammon.Location, to:Backgammon.Location, with:Backgammon.Dice.SingleDie)
}
typealias Capability = (descriptor:CapabilityDescriptor, action:() -> ())
protocol StoreSubscriber { func wasUpdated(store:Backgammon.Store) }

struct Backgammon {
    enum Color    { case black, white; fileprivate var direction:Direction { self == .black ? .desc : .asc } }
    enum Location { case unset, off, position(UInt), bar }
    let board: [Stone]
    let dice: Dice
    init() { self.init(.notStarted, startSetup(), Dice()) }
    func capabilities() -> [Capability] {
        let t = createToken()
        return applyFilter(allCaps()).compactMap {
            switch $0 {
            case .startNewGame                 : return ($0, { if t.redeem() { store.game = alter(.state(to:.started(playing:.white)))}})
            case .roll                         : return ($0, { if t.redeem() { store.game = store.game.alter(.roll) }})
            case let .switch(to:c)             : return ($0, { if t.redeem() { store.game = store.game.alter(.state(to:.started(playing:c)),
                                                                                                             .update(.dice(Dice()))) }})
            case let .move(c,_,.position(l1),d): return ($0, { if t.redeem() { store.game = store.game.alter(.update(.stone(Stone(color:c).alter(.set(.position(l1))))),
                                                                                                             .update(.dice(dice.alter(.invalidate(dice.first == d ? .first : .second))))) }})
            default : return nil
            }
        }
    }
    fileprivate let state: State
}
extension Backgammon {
    final class Store {
        init(game g:Backgammon) { game = g }
        var game:Backgammon { didSet { subscribers.forEach { $0.wasUpdated(store: self) } } }
        func register(subscriber s:StoreSubscriber) { subscribers = subscribers + [s] }
        private(set)
        var subscribers:[StoreSubscriber] = []
    }
}
extension Backgammon {
    struct Stone:Identifiable {
        enum Change { case set(Location) }
        let id: UUID
        let color: Backgammon.Color
        let location:Location
        init(color:Color) { self.init(UUID(),color, .unset) }
        private
        init(_ i: UUID, _ c:Color, _ l:Location) { id = i; color = c; location = l }
    }
}
extension Backgammon {
    struct Dice {
        init() { self.init(.unset, .unset) }
        let first: SingleDie
        let second: SingleDie
        enum SingleDie:Equatable  {
            case unset
            case unused(UInt)
            case used(UInt)
        }
    }
}
extension Backgammon.State: Equatable {}
extension Backgammon.Color: Equatable {}
extension Backgammon.Location: Equatable {}

//MARK: - private
fileprivate extension Backgammon {
    private func applyFilter(_ caps:[CapabilityDescriptor]) -> [CapabilityDescriptor] { [
        removeMovesForNoDiceWasCast,
        removeUnrepresentDice,
        removeUnrepresentMoves,
        removeSwitchColorForNotAllDieUnused,
        removeRollsForNotAllDiceUsed,
        removeSwitchColorForNotAllDieUnset,
        removeRollsForNotStartedYet,
        removeSwitchColorForNotStarted,
        removeOtherColorMoves,
        removeMovesForNoDiceLeft,
        removeRollsForNoDiceLeft,
        removeStartFromAlreadyStarted,
        removeSwitchColorForUsedDice,
        removeSwitchColorForNotAllDieUnused
    ].reduce(caps) { $1($0) } }
    private func removeStartFromAlreadyStarted(_ caps: [CapabilityDescriptor]) -> [CapabilityDescriptor] {
        caps.filter {
            switch ($0, state) {
            case (.startNewGame, .started): return false
            default: return true
            }
        }
    }
    private func removeOtherColorMoves(_ caps: [CapabilityDescriptor]) -> [CapabilityDescriptor] {
        caps.filter {
            switch ($0, state) {
            case let (.move(c0,_,_,_), .started(playing:c1)): return c0 != c1
            default: return true
            }
        }
    }
    private func removeSwitchColorForNotStarted(_ caps: [CapabilityDescriptor]) -> [CapabilityDescriptor] {
        caps.filter {
            switch ($0, state) {
            case (.switch, .notStarted): return false
            default: return true
            }
        }
    }
    private func removeSwitchColorForUsedDice(_ caps: [CapabilityDescriptor]) -> [CapabilityDescriptor] {
        caps.filter {
            switch ($0, state, dice.first, dice.second) {
            case let (.switch(to:c0), .started(playing:c1), .used,.used): return c0 != c1
            default: return true
            }
        }
    }
    private func removeSwitchColorForNotAllDieUnset(_ caps: [CapabilityDescriptor]) -> [CapabilityDescriptor] {
        caps.filter {
            switch ($0, state, dice.first, dice.second) {
            case (.switch, .started, .unset,_): return false
            case (.switch, .started, _,.unset): return false
            default: return true
            }
        }
    }
    private func removeSwitchColorForNotAllDieUnused(_ caps: [CapabilityDescriptor]) -> [CapabilityDescriptor] {
        caps.filter {
            switch ($0, state, dice.first, dice.second) {
            case (.switch, .started, .used,.used): return true // allow color switch

            case (.switch, .started, .unused,_): return false
            case (.switch, .started, _,.unused): return false
            default: return true
            }
        }
    }
    private func removeMovesForNoDiceLeft(_ caps: [CapabilityDescriptor]) -> [CapabilityDescriptor] {
        caps.filter {
            switch ($0,dice.first, dice.second) {
            case let (.move(_,_,_,.unused(d0)),.unused(d1), .used): return d0 == d1
            case let (.move(_,_,_,.unused(d0)),.used, .unused(d1)): return d0 == d1
            default: return true
            }
        }
    }
    private func removeMovesForNoDiceWasCast(_ caps: [CapabilityDescriptor]) -> [CapabilityDescriptor] {
        caps.filter {
            switch ($0,dice.first, dice.second) {
            case (.move,_, .unset): return false
            case (.move,.unset, _): return false
            default: return true
            }
        }
    }
    private func removeRollsForNoDiceLeft(_ caps: [CapabilityDescriptor]) -> [CapabilityDescriptor] {
        caps.filter {
            switch ($0,dice.first, dice.second) {
            case (.roll,.used,_): return false
            case (.roll,_,.used): return false
            default: return true
            }
        }
    }
    private func removeRollsForNotAllDiceUsed(_ caps: [CapabilityDescriptor]) -> [CapabilityDescriptor] {
        caps.filter {
            switch ($0,dice.first, dice.second) {
            case (.roll,.unused,_): return false
            case (.roll,_,.unused): return false
            default: return true
            }
        }
    }
    private func removeRollsForNotStartedYet(_ caps: [CapabilityDescriptor]) -> [CapabilityDescriptor] {
        caps
            .filter {
            switch ($0, state) {
            case (.roll, .notStarted): return false
            default: return true
            }
        }
    }
    private func removeUnrepresentMoves(_ caps: [CapabilityDescriptor]) -> [CapabilityDescriptor] {
        caps
            .filter {
            switch $0 {
            case let .move(c,.position(l0),_,_): return board.contains(where: { $0.color == c && $0.location == .position(l0)  })
            default: return true
            }
        }
    }
    private func removeUnrepresentDice(_ caps: [CapabilityDescriptor]) -> [CapabilityDescriptor] {
        caps
        .filter {
            switch ($0,dice.first, dice.second) {
            case let (.move(_,_,_,.unused(d0)),.unused(d1),.unused(d2)): return (d0 == d1) || (d0 == d2)
            case let (.move(_,_,_,.unused(d0)),  .used(d1),.unused(d2)): return (d0 == d1) || (d0 == d2)
            case let (.move(_,_,_,.unused(d0)),.unused(d1),  .used(d2)): return (d0 == d1) || (d0 == d2)
            case     (.move(_,_,_,.unused    ),  .used,      .used    ): return false
            default: return true
            }
        }
    }
}

fileprivate extension Backgammon {
    enum State { case notStarted, started(playing:Backgammon.Color) }
    enum Direction { case asc, desc }
}
private extension Backgammon {
    private enum Change {
        case state(to:State)
        case update(_Update); enum _Update { case stone(Stone), dice(Dice) }
        case roll
    }
    private init(_ s:State, _ b:[Stone], _ d:Dice) {
        state = s; board = b; dice = d
    }
    private func numberOfEnemyStonesOn(point:UInt, color:Backgammon.Color) -> UInt {
        UInt(board.filter { $0.color != color && $0.location == .position(point) }.count)
    }
    private func allCaps() -> [CapabilityDescriptor] {
        [.startNewGame,.switch(to:.white),.switch(to:.black),.roll] + board.map { $0.capabilitiesDescriptions() }.flatMap{ $0 }
    }
    private func alter(_ cs:Change...) -> Self { cs.reduce(self) { $0.alter($1) } }
    private func alter(_ c:Change) -> Self {
        switch c {
        case .state (to:    let state): return .init(state, board,                               dice             )
        case .update(.stone(let s   )): return .init(state, board.filter{ $0.id != s.id } + [s], dice             )
        case .update(.dice (let dice)): return .init(state, board,                               dice             )
        case .roll                    : return .init(state, board,                               dice.alter(.roll))
        }
    }
}
private extension Backgammon {
    typealias ExclusitivityToken = (redeem:() -> Bool, invalidate:() -> ())
    func createToken() -> ExclusitivityToken {
        var _token = true
        return (
            redeem    : { defer { _token = false }; return _token },
            invalidate: {         _token = false                  })
    }
}
fileprivate extension Backgammon.Dice {
    enum Change{
        case roll
        case invalidate(_Die)
        enum _Die { case first , second }
    }
    
    func alter(_ c:Change) -> Self {
        switch c  {
        case .roll: return .init(.unused((1 ..< 7).randomElement()!), .unused((1 ..< 7).randomElement()!))
        case .invalidate(.first ):  if case let .unused(x) = first  {  return .init(.used(x),second) }; return self
        case .invalidate(.second):  if case let .unused(x) = second {  return .init(first, .used(x)) }; return self
        }
    }
    private
    init(_ f: SingleDie, _ s: SingleDie) { first = f; second = s }
    func faces() -> (UInt?, UInt?) {
        let a:UInt?
        let b:UInt?
        if case let .unused(x) = first  { a = x } else { a = nil }
        if case let .unused(x) = second { b = x } else { b = nil }
        return (a,b)
    }
}
private extension Backgammon.Stone {
    func alter(_ changes: [Change] ) -> Self { changes.reduce(self) { $0.alter($1) } }
    func alter(_ c:Change) -> Self {
        switch c {
        case .set(let location): return .init(id,color,location)
        }
    }
    func capabilitiesDescriptions() -> [CapabilityDescriptor] {
        switch location {
        case .unset,.off: return []
        case .position:
           return (1..<7).map { d in (advance(by: d), d )}
                .compactMap { (s,d) in
                    switch s.location {
                    case .position: return .move(color, from:location, to:s.location, with:.unused(d))
                    default: return nil
                    }
                }
        case .bar: return [] }
    }
}
extension Backgammon.Stone {
    private func advance(by:UInt) -> Self {
        switch location {
        case .unset, .off: return self
        case .position(let uInt):
            switch color.direction {
            case .asc : return {  let d = uInt + by ; return d < 24 ? .init(id, color, .position(d)) : self }()
            case .desc: return {  let d = Int(uInt) - Int(by) ; return d > 00 ? .init(id, color, .position(UInt(d))) : self }()
            }
        case .bar:
            return .init(id, color, .position(color.direction == .asc ? 0 + by : 24 - by))
        }
    }
}
private func startSetup() -> [Backgammon.Stone] {
    return [
        .init(color: .white).alter(.set(.position(0))),
        .init(color: .white).alter(.set(.position(0))),
        
            .init(color: .black).alter(.set(.position(5))),
        .init(color: .black).alter(.set(.position(5))),
        .init(color: .black).alter(.set(.position(5))),
        .init(color: .black).alter(.set(.position(5))),
        .init(color: .black).alter(.set(.position(5))),
        
            .init(color: .black).alter(.set(.position(7))),
        .init(color: .black).alter(.set(.position(7))),
        .init(color: .black).alter(.set(.position(7))),
        
            .init(color: .white).alter(.set(.position(11))),
        .init(color: .white).alter(.set(.position(11))),
        .init(color: .white).alter(.set(.position(11))),
        .init(color: .white).alter(.set(.position(11))),
        .init(color: .white).alter(.set(.position(11))),
        
            .init(color: .black).alter(.set(.position(12))),
        .init(color: .black).alter(.set(.position(12))),
        .init(color: .black).alter(.set(.position(12))),
        .init(color: .black).alter(.set(.position(12))),
        .init(color: .black).alter(.set(.position(12))),
        
            .init(color: .white).alter(.set(.position(16))),
        .init(color: .white).alter(.set(.position(16))),
        .init(color: .white).alter(.set(.position(16))),
        
            .init(color: .white).alter(.set(.position(18))),
        .init(color: .white).alter(.set(.position(18))),
        .init(color: .white).alter(.set(.position(18))),
        .init(color: .white).alter(.set(.position(18))),
        .init(color: .white).alter(.set(.position(18))),
        
            .init(color: .black).alter(.set(.position(23))),
        .init(color: .black).alter(.set(.position(23)))
    ]
}
