//
//  ContentView.swift
//  DeclarativeBG
//
//  Created by Manuel Meyer on 10.05.22.
//

import SwiftUI

extension Collection {
    public func sorted<Value: Comparable>(on property: KeyPath<Element, Value>, by areInIncreasingOrder: (Value, Value) -> Bool) -> [Element] {
        sorted { currentElement, nextElement in
            areInIncreasingOrder(currentElement[keyPath: property], nextElement[keyPath: property])
        }
    }
}

struct ContentView: View {
    init(viewState v: ViewState) { viewState = v }
    var body: some View {
        List {
            Section {
                ForEach(Array(zip(0..<viewState.board.count,viewState.board.sorted(on: \.key, by: <))) , id:\.0) { pair in
                    HStack {
                        Text(String(describing: pair.1.0))
                        ForEach(pair.1.1, id: \.id) {
                           Text( $0.color == .black ? "black" : "white")
                        }
                    }
                }
            }
            Section {
                Text(String(describing: viewState.dice)).minimumScaleFactor(0.01)

            }
            Section {
                ForEach(Array(zip(0..<viewState.capabilities.count,viewState.capabilities)) , id:\.0) { pair in
                    Button { pair.1.action() } label: { Text(String(describing: pair.1)).minimumScaleFactor(0.01).lineLimit(3) }
                }
            }
        }
    }
    @ObservedObject
    private var viewState  : ViewState
}

final class ViewState: ObservableObject, StoreSubscriber {
    @Published var capabilities: [Capability]
    @Published var board: [UInt: [Backgammon.Stone]]
    @Published var dice: Backgammon.Dice
    init(store s:Backgammon.Store) {
        capabilities = s.game.capabilities()
        board = s.game.board.reduce([:]) {
            switch $1.location {
            case .position(let p): var x = $0; x[p] = (x[p] ?? []) + [$1]; return x
            default: return $0
            }
        }
        dice = s.game.dice
        s.register(subscriber:self)
    }
    func wasUpdated(store:Backgammon.Store) {
        capabilities = store.game.capabilities()
        dice = store.game.dice
        board = store.game.board.reduce([:]) {
            switch $1.location {
            case .position(let p): var x = $0; x[p] = (x[p] ?? []) + [$1]; return x
            default: return $0
            }
        }
    }
}
func string(for d:Backgammon.Dice) -> String {
    switch (d.first,d.second) {
    case     (.unset,    .unset    ): return "please role dice…"
    case let (.unused(x),.unused(y)): return "unused:\(x) unused:\(y)"
    case let (  .used(x),.unused(y)): return   "used:\(x) unused:\(y)"
    case let (.unused(x),  .used(y)): return "unused:\(x) used:\(y)"
    case let (  .used(x),  .used(y)): return   "used:\(x) used:\(y)"

    default : return ""
    }
}
func string(for c:Capability) -> String {
    switch c {
    
    case (.startNewGame,_): return "start new game"
    case (let .move(color, from: from, to: to, with:with),_): return "move \(color == .black ? "black" : "white") from:\(from) to:\(to) with:\(with)"
    case (.roll,_): return "roll dice"
        
    case (.switch(to: let to), _): return "switch player to \(to == .white ? "white" : "black")"
        
    }
}
